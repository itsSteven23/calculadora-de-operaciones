document.getElementById('calculator-form').addEventListener('submit', function (e) {
    e.preventDefault();

    const number1 = parseFloat(document.getElementById('number1').value);
    const number2 = parseFloat(document.getElementById('number2').value);

    const sumResult = number1 + number2;
    const subtractResult = number1 - number2;
    const multiplyResult = number1 * number2;
    const divideResult = number1 / number2;
    const modResult = number1 % number2;

    document.getElementById('sum').textContent = `Suma: ${sumResult}`;
    document.getElementById('subtract').textContent = `Resta: ${subtractResult}`;
    document.getElementById('multiply').textContent = `Multiplicación: ${multiplyResult}`;
    document.getElementById('divide').textContent = `División: ${divideResult}`;
    document.getElementById('mod').textContent = `Módulo: ${modResult}`;
});
